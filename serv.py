from flask import Flask, request, render_template, send_from_directory

app = Flask(__name__)

@app.route("/")
def home():
    return render_template("index.html")


@app.route("/calculate", methods=["POST"])
def calc():
    content = request.json
    if "qn" in content:
        output = eval(content["qn"])
        return {"value":output}


@app.route("/<path:filename>")
def send_file(filename):
    return send_from_directory(app.static_folder, filename)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=9000, debug=True)